package com.example.sanket.punchout.listwifi;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Created by sanket on 7/8/2016.
 * Unit tests for the implementation of {@link ListWifiPresenter}
 */
public class ListWifiPresenterTest {

    @Mock
    private ListWifiContract.View mListWifiView;

    private ListWifiPresenter mListWifiPresenter;

    @Before
    public void setupListWifiPresenter() {
        MockitoAnnotations.initMocks(this);

        mListWifiPresenter = new ListWifiPresenter(mListWifiView);
    }

    @Test
    public void clickOnWifiItem_ShowsDurationUi() {
        //On selecting one of the wifi item from the list
        mListWifiPresenter.saveWifi(0);

        //verify that show duration screen is shown
        Mockito.verify(mListWifiView).showDuration();
    }

    @Test
    public void fetchAvailableWifiList_AndLoadIntoView() {
        mListWifiPresenter.fetchWifiList();


        //verify that the list is loaded into view
        Mockito.verify(mListWifiView).showWifiList();
    }

}
