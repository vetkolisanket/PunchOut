package com.example.sanket.punchout.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by sanket on 7/8/2016.
 * Model class for Wifi
 */
public class Wifi {

    @NonNull
    private String name;

    @Nullable
    private String type;

    public Wifi(@NonNull String name) {
        this(name, null);
    }

    public Wifi(@NonNull String name,@Nullable String type) {
        this.name = name;
        this.type = type;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Nullable
    public String getType() {
        return type;
    }
}
