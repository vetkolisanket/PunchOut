package com.example.sanket.punchout.listwifi;

import com.example.sanket.punchout.data.Wifi;

import java.util.List;

/**
 * Created by sanket on 7/8/2016.
 */
public interface ListWifiContract {

    interface View {

        void setProgressIndicator(boolean active);

        void showNoWifiPlaceholder();

        void showWifiList();

        void showDuration();
    }

    interface UserActionsListener {
        void saveWifi(int position);

        List<Wifi> fetchWifiList();
    }

}
