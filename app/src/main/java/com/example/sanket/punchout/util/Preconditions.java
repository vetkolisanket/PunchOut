package com.example.sanket.punchout.util;

import android.support.annotation.NonNull;

/**
 * Created by sanket on 7/8/2016.
 */
public class Preconditions {

    public static <T> T checkNotNull(T reference, @NonNull String errorMessage) {
        if (reference == null) {
            throw new NullPointerException(errorMessage);
        }
        return reference;
    }

}
