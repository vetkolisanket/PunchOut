package com.example.sanket.punchout.listwifi;

import android.support.annotation.NonNull;

import com.example.sanket.punchout.data.Wifi;
import com.google.common.base.Preconditions;

import java.util.List;

/**
 * Created by sanket on 7/8/2016.
 */
public class ListWifiPresenter implements ListWifiContract.UserActionsListener {

    private final ListWifiContract.View mListWifiView;

    public ListWifiPresenter(@NonNull ListWifiContract.View listWifiView) {
        this.mListWifiView = Preconditions.checkNotNull(listWifiView, "listWifiView cannot be null!");
    }

    @Override
    public void saveWifi(int position) {
        mListWifiView.showDuration();
    }

    @Override
    public List<Wifi> fetchWifiList() {
        return null;
    }


}
